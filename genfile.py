import openai, os

API_key = "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"
openai.api_key = API_key

def storygen(prompt):
    print(prompt)
    query = openai.Completion.create(
		engine="text-davinci-003",
		prompt=f"create an acceptance criteria in Gherkins from the below requirements\n {prompt}",
		max_tokens=1024,
		n=1,
		stop=None,
		temperature=0.7,
	)
    response = query.choices[0].text
    print(response)
    with open("./tempfiles/userstory.txt", "w") as f:  
        f.write("\nAC1" + response)
        f.close()
    return response

