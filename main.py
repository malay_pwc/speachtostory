from flask import Flask, render_template, request, Response, jsonify, make_response, send_file
from voiceinput import voice_input, storygen
from flask_cors import CORS
import json,os
from testcase_generator import generate_testcase , excel_write

app = Flask(__name__, root_path='tempfiles/')
inputfile = os.path.join(app.root_path, 'userstory.txt')

CORS(app)

CORS(app, resources={r"/*": {"origins": "http://localhost:3000"}})


@app.route('/voice', methods=['GET','POST'])
def index():
    print("welcome to User story & Test case generator")
    res = voice_input()
    # print("voc", voc)
    return res

@app.route('/story', methods=['GET','POST'])
def gen_story():
    text = request.json
    print(text['text'])
    story = storygen(text['text'])
    # print("voc", voc)
    return story

@app.route('/report', methods=['GET'])
def generate_file():
    testcase_out = generate_testcase()
    # with open("./sample.json",'r') as json_file:
    #         testcase_out = json_file.read()  
    return testcase_out

@app.route('/download_report', methods=['GET'])
def download_xls():
    excel_write()
    temp_file = open("./demo_test.xlsx", 'rb')
    response = make_response(send_file(temp_file, download_name=''))
    return response
    
# app.run()
if __name__ == "__main__":
    print("Welcome")
    app.run(host="0.0.0.0", port=5000, debug=True)