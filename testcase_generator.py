import openai
from openai import ChatCompletion
import xlsxwriter
import json

openai.api_key = "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"

def generate_testcase():

    with open("./tempfiles/userstory.txt", "r", encoding="utf-8") as f:
        user_story =f.read()
    mystr = f'''Create minimum of five different test cases for the below user story\n + {user_story} + and provide the 
                Test case name, 
                Preconditions, 
                Test Steps, 
                Expected Results, 
                testcase type as positive or negative.
                format the answer as a JSON object with the keys (testcase name, Preconditions,Test Steps, Expected Results, testcase type) '''

    response = ChatCompletion.create(
                model="gpt-3.5-turbo",
                messages=[{"role": "system", "content": mystr}]
                )

    result = response.choices[0].message.content
    # print(result)
    try:
        
        result_final = result.replace('\\n','$')
        dict_input_xls = json.loads(result_final)
        print(result_final)
    except Exception as e:
        print("except block")
        generate_testcase()
    with open("./sample.json",'w') as json_file:
        json_file.write(result_final)
    return result_final
    '''with open(testcase_filepath,"w") as F:
        F.write(result)
        F.close()
    print("test cases generated from prompt successfully")

    try:
        # global dict_input_xls
        with open(testcase_filepath,"r",encoding="utf-8") as f:
            input_file = f.read().split('\n')

        count = 0
        for i in range(0,len(input_file)):
            if str(input_file[i][:9]).lower() == 'test case':
                output =  input_file
                break
            else:
                count += 1
        
        if count == len(input_file):
            print('Wrong format')
            global wrong_format_count
            wrong_format_count += 1
            time.sleep(10)
            if wrong_format_count <= 3:
                generate_testcase()
        else:
            pass
        
        with open('test_case.txt','w') as f:
            for i in range(0,len(input_file)):
                if i>= count:
                    f.write(input_file[i]+'\n')
        f.close()

        with open('test_case.txt','r') as f:
            input_file_cleaned = f.read().split('\n')
        print(input_file_cleaned)

        no_of_tc = input_file_cleaned.count('Test Steps:')

        tc_ranges = [0]
        for i in range(1,no_of_tc):
            tc_ranges.append(input_file_cleaned.index(f'Test Case {i+1}:') - input_file_cleaned.index(f'Test Case {i}:')-1)
        last_range = len(input_file_cleaned)-sum(tc_ranges)
        tc_ranges.append(last_range)

        tc_details_dict = {}
        for i in range(1,no_of_tc+1):
            tc_details_dict[f'Test Case {i}'] = input_file_cleaned[sum(tc_ranges[0:i])+i:sum(tc_ranges[0:i+1])+i]

        # print(tc_details_dict)

        for tc_name in tc_details_dict:
            inner_content_dict = {}
            # print(tc_name)
            tc1_testcase = tc_details_dict[tc_name]
            # print(tc1_testcase)
            Preconditions_index = tc1_testcase.index('Preconditions:')
            Test_Steps_index = tc1_testcase.index('Test Steps:')
            Expected_Results_index = tc1_testcase.index('Expected Results:')
            # print(Preconditions_index,Test_Steps_index,Expected_Results_index)
            # print(tc1_testcase[Preconditions_index+1:Test_Steps_index])
            inner_content_dict[tc_details_dict[tc_name][Preconditions_index]]=tc1_testcase[Preconditions_index+1:Test_Steps_index] 
            inner_content_dict[tc_details_dict[tc_name][Test_Steps_index]]=tc1_testcase[Test_Steps_index+1:Expected_Results_index] 
            inner_content_dict[tc_details_dict[tc_name][Expected_Results_index]]=tc1_testcase[Expected_Results_index+1:] 
            #print(f'this is :{inner_content_dict}')
            #print(f'lets see: {tc_details_dict}')
            tc_details_dict[tc_name]=inner_content_dict
        print("line no 103",tc_details_dict)
        dict_input_xls = json.dumps(tc_details_dict)
        print(dict_input_xls)
        with open("./sample.json",'w') as json_file:
            json_file.write(dict_input_xls)

        return input_file_cleaned

    except Exception as E:
        print(E)
        time.sleep(10)
        generate_testcase()'''
       
def excel_write():
    with open ("./sample.json") as json_file:
        json_file_content = json_file.read()
    dict_input_xls = json.loads(json_file_content)
    # print(dict_input_xls)
    workbook = xlsxwriter.Workbook('demo_test.xlsx')
    worksheet = workbook.add_worksheet()
    row = 0
    col = 0
    upper_row = 0 #temp
    last_row = [] 
    for x in dict_input_xls.items():
        # print(x[0]) 
        worksheet.write(row, col,     str(x[0]),workbook.add_format({'bold':True,'bg_color':'orange','font_color':'white'}))   
        for i in x[1].items(): #inner dictiory
            # print(i)
            worksheet.write(row, col+1, str(i[0]),workbook.add_format({'bold':True,'bg_color':'orange','font_color':'white'}))
            for j in i[1].split('$'):  #inner list of tuple
                # print(j)         
                worksheet.write(row+1, col+1, str(j))
                row += 1
                last_row.append(row)
            row = upper_row  #temp row = 0
            col+=1
        row = max(last_row) + 1
        col = 0
        upper_row = max(last_row) +1 #temp
    workbook.close()
    return 'excel  generated'

