import speech_recognition as sr
# from genfile import storygen
import openai, os

API_key = "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"
openai.api_key = API_key
# global said
# said = ''
def voice_input():
    said = ''
    r = sr.Recognizer()
    with sr.Microphone() as source:
        audio = r.listen(source)
        
        try:
            said = r.recognize_google(audio)
            print(said)
            # gen_story = storygen(said)
        except Exception as e:
            print('Exception: ' + str(e))
    return said

def storygen(text):
    # print(prompt)
    query = openai.Completion.create(
		engine="text-davinci-003",
		prompt=f"create an acceptance criteria in Gherkins from the below requirements\n {text}",
		max_tokens=1024,
		n=1,
		stop=None,
		temperature=0.7,
	)
    response = query.choices[0].text
    print(response)
    with open("./tempfiles/userstory.txt", "w") as f:  
        f.write("\nAC1" + response)
        f.close()
    return response



